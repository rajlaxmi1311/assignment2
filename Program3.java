import java.io.*;
class Demo{

	public static void main (String [] args)throws IOException{

		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));

		System.out.print("Enter Number : ");
		int num = Integer.parseInt(br.readLine());

		if(num%2==1){
		
			System.out.println("Number is odd");
		}
		else{
		
			System.out.println("Number is even ");
		}
	}
}
