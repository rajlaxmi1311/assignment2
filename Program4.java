
import java.io.*;
class Demo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.print("Enter starting range : ");
		int startRange = Integer.parseInt(br.readLine());

		System.out.print("Enter ending range : ");
		int endRange = Integer.parseInt(br.readLine());

		for (int i = startRange; i<= endRange ; i++){
		
			if(i%2==1){
			
				System.out.println(i);
			}
		}
	}
}
